(setq org-agenda-include-all-todo t)
(setq org-agenda-include-diary t)

;;open agenda in current window
(setq org-agenda-window-setup (quote current-window))

(setq org-fast-tag-selection-single-key t)
(setq org-use-fast-todo-selection t)
(setq org-startup-truncated nil)

;; Set the times to display in the time grid
(setq org-agenda-time-grid
      (quote
       ((daily today remove-match)
        (800 1200 1600 2000)
        "......" "----------------")))

;; Set default column view headings: Task Effort Clock_Summary
;; (setq org-columns-default-format
;;       "%50ITEM(Task) %10Effort(Effort){:} %10CLOCKSUM %16TIMESTAMP_IA")

;; seting up the todo flags
(setq org-log-done t)

(setq org-directory "~/org/")
(setq org-default-notes-file "~/org/notes.org")

(setq org-agenda-files (list "~/org/work.org"
                             "~/org/refile.org"
                             "~/org/someday.org"
                             "~/org/personal.org"))

;; Targets include this file and any file contributing to
;; the agenda - up to 9 levels deep
(setq org-refile-targets (quote ((nil :maxlevel . 1)
                                 (org-agenda-files :maxlevel . 1))))

(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
              (sequence "ISSUE(i)" "|" "FIXED(f)")
              (sequence "WAITING(w@/!)" "|"
                        "CANCELLED(c@/!)" "PHONE" "MEETING"))))


;;Capture templates for: TODO tasks, Notes, appointments, phone calls, meetings,
;;and org-protocol
(setq org-capture-templates
      (quote (("t" "Todo" entry (file "~/org/refile.org")
               "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)

              ("r" "Respond" entry (file "~/org/refile.org")
               "* NEXT Respond to %:from on %:subject\nSCHEDULED: %t\n%U\n%a\n"
               :clock-in t :clock-resume t :immediate-finish t)

              ("n" "Note" entry (file "~/org/note.org")
               "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)

              ("j" "Journal" entry (file+datetree "~/org/journal.org")
               "* %?\n%U\n" :clock-in t :clock-resume t)

              ("w" "Org-protocol" entry (file "~/org/refile.org")
               "* TODO Review %c\n%U\n" :immediate-finish t)

              ("m" "Meeting" entry (file "~/org/refile.org")
               "* MEETING with %? :MEETING:\n%U" :clock-in t :clock-resume t)

              ("a" "Appointment" entry (file "~/org/refile.org")
               "* APPT %? %^G\n  %^t")

              ("p" "Phone call" entry (file "~/org/refile.org")
               "* PHONE %? :PHONE:\n%U" :clock-in t :clock-resume t)

              ("h" "Habit" entry (file "~/org/refile.org")
               "* NEXT %?\n%U\n%a\nSCHEDULED: %(format-time-string
\"%<<%Y-%m-%d %a .+1d/3d>>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE:
NEXT\n:END:\n"))))


;;set priority range from A to C with default A
(setq org-highest-priority ?A)
(setq org-lowest-priority ?C)
(setq org-default-priority ?A)

;;set colours for priorities
(setq org-priority-faces '((?A . (:foreground "#F0DFAF" :weight bold))
                           (?B . (:foreground "LightSteelBlue"))
                           (?C . (:foreground "OliveDrab"))))

  (setq org-tag-persistent-alist
        '((:startgroup . nil)
          ("@home" . ?h)
          ("@School" . ?f)
          ("@work" . ?t)
          (:endgroup . nil)
          (:startgroup . nil)
          ("Pentest" . ?o)
          ("Coding" . ?d)
          ("Electronic" . ?d)
          (:endgroup . nil)
          ("Project" . ?k)
          ("Report" . ?d)
          ("Reading" . ?k)
          ("Laptop" . ?k)
          ("Serie" . ?k)
          ("Movie" . ?k)
          ("GoingOut" . ?k)
          ("Climbing" . ?k)
          ("Holiday" . ?k)
          ("Bill" . ?b)
          ("Idear" . ?b)
          )
        )

  (setq org-tag-faces
        '(
          ("@home" . (:foreground "GoldenRod" :weight bold))
          ("@school" . (:foreground "GoldenRod" :weight bold))
          ("@work" . (:foreground "GoldenRod" :weight bold))
          ("Pentest" . (:foreground "IndianRed1" :weight bold))
          ("Coding" . (:foreground "IndianRed1" :weight bold))
          ("Electronic" . (:foreground "IndianRed1" :weight bold))
          )
        )

(setq org-stuck-projects
      '("+PROJECT/-MAYBE-DONE" ("NEXT" "TODO") ("@shop")
        "\\<IGNORE\\>"))

; Compact the block agenda view
(setq org-agenda-compact-blocks t)

;; (setq org-agenda-custom-commands
;;       `(("N" "Notes" tags "NOTE"
;;          ((org-agenda-overriding-header "Notes")
;;           (org-tags-match-list-sublevels t)))

;;         ("r" "Tasks to Refile" tags "REFILE"
;;          ((org-agenda-overriding-header "Notes and Tasks to Refile")
;;           (org-agenda-overriding-header "Tasks to Refile")))

;;         ("h" "Habits" tags-todo "STYLE=\"habit\""
;;          ((org-agenda-overriding-heaaader "Habits")
;;           (org-agenda-sorting-strategy
;;            '(todo-state-down effort-up category-keep))))

;;         ("X" "Agenda" ((agenda "") (alltodo))
;; 	       (
;; 	        (org-agenda-start-on-weekday nil)
;; 	        (org-agenda-start-with-log-mode t)
;;           (org-agenda-log-mode-items '(closed clock state)))

;;         ("p" "Projects" tags-todo "Project"
;;          ((org-agenda-overriding-header "Projects"))

;;           (org-agenda-sorting-strategy
;;            '(category-keep))))
;;         ))

;; (setq org-agenda-custom-commands
;;       '(("c" agenda "Simple agenda view"
;;          ((org-agenda-span 'day))
;; 				(tags-todo "Project"
;; 					   ((org-agenda-overriding-header "Projects:")
;;               ))
;;           ))))

(add-to-list 'org-agenda-custom-commands
             '("b" agenda "Today's Deadlines"
               ((org-agenda-span 'day))
               (tags-todo "Project"
               					  ((org-agenda-overriding-header "Projects:")
                           ))

               ))
(provide 'setup-agenda)
