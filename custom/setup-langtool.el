;; This buffer is for text that is not saved, and for Lisp evaluation.
;; To create a file, visit it with <open> and enter text in its buffer.

(use-package langtool
	:ensure t
	:config (setq langtool-language-tool-jar "~/.emacs.d/LanguageTool-4.4/languagetool-commandline.jar")
  (setq langtool-default-language "en-US")
  ;; (add-hook 'org-mode-hook
  ;;         (lambda ()
  ;;            (add-hook 'after-save-hook 'langtool-check nil 'make-it-local)))
  )
