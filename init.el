(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))

  ;; Comment/uncomment these two lines to enable/disable MELPA and
  ;;MELPA Stable as desired
  (add-to-list
   'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)

  ;;(add-to-list 'package-archives (cons "melpa-stable"
  ;; (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)

    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives
                 '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))

    (add-to-list 'package-archives
             '("org" . "http://orgmode.org/elpa/") t) ;
    (add-to-list 'package-archives
                 '("elpa" . "http://elpa.gnu.org/packages/") t)

(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(add-to-list 'load-path "~/.emacs.d/custom")
(add-to-list 'load-path "~/.emacs.d/libs")

;; -------------------- REQUIRES --------------------

(require 'setup-general)
(require 'setup-helm)
(require 'setup-vhdl)
(require 'setup-magit)
(require 'setup-autopair)
(require 'setup-yasnippet)
(require 'setup-flycheck)
(require 'setup-c)
(require 'setup-bash)
(require 'setup-format)
(require 'setup-compile)
(require 'setup-lua)
(require 'setup-org)
(require 'setup-python)
(require 'setup-debug)
(require 'setup-latex)
(require 'setup-company)
(require 'setup-editing)
(require 'setup-minimap)
(require 'setup-bison)
(require 'setup-w3m)
(require 'setup-ggtags)
(require 'setup-narrow)
(require 'setup-emms)
(require 'setup-doxymacs)
(require 'setup-java)
(require 'setup-god-mode)
(require 'setup-awk)
(require 'setup-safe-local-variables)
(require 'setup-rust)
(require 'setup-ocaml)
(require 'setup-erc)
(require 'setup-arduino)
(require 'setup-agenda)
;;(require 'setup-R)
;;(require 'setup-mu4e)
